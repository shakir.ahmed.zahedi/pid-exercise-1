package Excercise;

import java.util.Scanner;
/**
 * This is a  Main class.
 * this class interact with user.
 * @author  Shakir Ahmed Zahedi
 * @version 1.0
 * @since   2020-03-16
 */

public class Main {
    public static void main(String[] args) {
        ProductOrganizer productOrganizer= new ProductOrganizer();
        Scanner scanner=new Scanner(System.in);
        System.out.println(">> ******************************************************");
        System.out.println(">> *****************     WELCOME    *********************");
        System.out.println(">> ******************************************************");

        boolean found=true;
        while (found) {
            System.out.println(">> Do you want to Continue:\n"+
                    ">> press 'y' to yes and press 'n' to no\n");
            char continueOption = scanner.next().charAt(0);
            if(continueOption=='y') {

                System.out.println(">> Pick an option:\n" +
                        ">> (1) Show Bars List\n" +
                        ">> (2) Show Bars List Based On Highest Protein Content\n" +
                        ">> (3) Show Bars List Based On Highest Fatt Content\n" +
                        ">> (4) Show Bars user limit List Based On Highest Fiber Content\n" +
                        ">> (5) Filter Bars Which Has More Than User Input Protein And Review ByUser And Sort Them " +
                        "From Highest To Lowest\n" +
                        ">> (6) Save and Quit");
                //Scanner scanner=new Scanner(System.in);
                int choseOption = scanner.nextInt();

                if (choseOption == 1) {
                    System.out.println(" Your List:");
                    System.out.println(" **********************");
                    productOrganizer.showTheNameOfAllBars();
                    found=true;

                } else if (choseOption == 2) {
                    System.out.println(" Your List:");
                    System.out.println(" **********************");
                    System.out.println("BarName --- Protein Content");

                    productOrganizer.sortBarsBasedOnHighestProteinContent();
                    found=true;
                } else if (choseOption == 3) {
                    System.out.println(" Your List:");
                    System.out.println(" **********************");
                    System.out.println("BarName --- Fatt Content");
                    productOrganizer.sortBarsBasedOnHighestFatContent();
                    found=true;
                }else if (choseOption == 4) {
                    System.out.println("Enter the limit:");
                    double limit=scanner.nextDouble();
                    System.out.println(" Your List:");
                    System.out.println(" **********************");
                    System.out.println("BarName --- Fiber Content");
                    productOrganizer.FilterBarsWhichHasLessThanUserInputFiberAndSortThemFromHighestToLowest(limit);
                    found = true;
                }else if (choseOption == 5) {
                    System.out.println("Enter the limit:");
                    double limit=scanner.nextDouble();
                    System.out.println("Enter the Reviewer:");
                    String reviewer=scanner.next();
                    System.out.println(" Your List:");
                    System.out.println(" **********************");
                    System.out.println("BarName --- Protein Content");
                    productOrganizer.FilterBarsWhichHasMoreThanUserInputProteinAndReviewByUserAndSortThemFromHighestToLowest(limit,reviewer);
                    found = true;
                }
                else if (choseOption == 6) {
                    found=false;
                }
                else {
                    System.out.println("You need to enter a valid key!");
                    found = true;
                }
            }
            else if (continueOption=='n') {
                found = false;
                break;

            }
            else {
                System.out.println("You need to enter a valid key!");
                found = true;
            }
        }

        System.out.println("*********************************************************************");
        System.out.println("Thank you for use the Application");
        System.out.println("*********************************************************************");
        scanner.close();
        System.exit(0);

    }
}