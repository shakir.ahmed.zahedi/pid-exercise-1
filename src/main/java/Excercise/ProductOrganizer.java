package Excercise;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;


import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This is a  ProductOrganizer class.
 * this class show  protien bars information to the customer from the avialable bars.
 *
 * @author Shakir Ahmed Zahedi
 * @version 1.0
 * @since 2020-03-16
 */

public class ProductOrganizer {
    //fields
    private ArrayList<Product> products;
    private ArrayList<Reviewer> reviewers;

    public ProductOrganizer() {
        products = new ArrayList<>();
        reviewers = new ArrayList<>();
    }

    /**
     * This method will read all the products from  the file.
     */

    public void viewProducts() {
        SAXBuilder builder = new SAXBuilder();
        File xmlFile = new File("bars.xml");

        try {
            Document document = (Document) builder.build(xmlFile);
            Element rootNode = document.getRootElement();
            List<Element> productList = rootNode.getChildren();


            for (int i = 0; i < productList.size(); i++) {
                reviewers.clear();
                Element ProductNode = (Element) productList.get(i);
                List<Element> reviewerList = ProductNode.getChildren();


                for (Element e : reviewerList) {
                    if (e.getName().equals("review")) {
                        List<Element> reviewList = e.getChildren();
                        // for (Element el : e.getChildren()) {
                        for (int j = 0; j < reviewList.size(); j++) {
                            reviewers.add(new Reviewer(e.getChildren().get(j).getAttributeValue("personID"),
                                    LocalDate.parse(e.getChildren().get(j).getChildText("date")),
                                    Integer.parseInt(e.getChildren().get(j).getChildText("score"))));
                        }
                    }
                }

                products.add(new Product(ProductNode.getAttributeValue("SN"),
                        Double.parseDouble(ProductNode.getChildText("fett")),
                        Double.parseDouble(ProductNode.getChildText("energy")),
                        Double.parseDouble(ProductNode.getChildText("kolhydrat")),
                        Double.parseDouble(ProductNode.getChildText("protein")),
                        Double.parseDouble(ProductNode.getChildText("fiber")),
                        new ArrayList<>(reviewers)));

            }

        } catch (IOException io) {
            System.out.println(io.getMessage());
        } catch (JDOMException jdomex) {
            System.out.println(jdomex.getMessage());
        }


    }

    /**
     * This method will show all the name of the bar
     */


    public void showTheNameOfAllBars() {
        products.clear();
        viewProducts();
        List<String> sortedList = products.stream()
                .map(Product::getSN)
                .collect(Collectors.toList());
        sortedList.forEach(System.out::println);

    }

    /**
     * This method will  take user input and Sort bars  based on highest fiber content
     * with in limit provide by the user .
     */

    public void FilterBarsWhichHasLessThanUserInputFiberAndSortThemFromHighestToLowest(double limit) {
        products.clear();
        viewProducts();
        List<String> sortedList = products.stream()
                .filter((bar -> bar.getFiber() < limit))
                .sorted(Comparator.comparing(Product::getFiber).reversed())
                .map(bar -> bar.getSN() + " --- " + bar.getFiber())
                .collect(Collectors.toList());
        sortedList.forEach(System.out::println);

    }

    public void FilterBarsWhichHasMoreThanUserInputProteinAndReviewByUserAndSortThemFromHighestToLowest(double limit, String name) {
        products.clear();
        viewProducts();
        products
                .stream()
                .filter(bar -> bar.getProtein() > limit && bar.getReviewers().stream().anyMatch(review -> review.getPersonID().equals(name)))
                .sorted(Comparator.comparing(Product::getProtein).reversed())
                .forEach(bar -> System.out.println(bar.getSN()+" --- "+bar.getProtein()));

    }


    /**
     * This method will Sort bars  based on highest protein content.
     */


    public void sortBarsBasedOnHighestProteinContent() {
        products.clear();
        viewProducts();
        List<String> sortedList = products.stream()
                .sorted(Comparator.comparing(Product::getProtein).reversed())
                .map(bar -> bar.getSN() + " --- " + bar.getProtein())
                .collect(Collectors.toList());
        sortedList.forEach(System.out::println);

    }

    /**
     * This method will Sort bars based on highest fat content.
     */

    public void sortBarsBasedOnHighestFatContent() {
        products.clear();
        viewProducts();
        List<String> sortedList = products.stream()
                .sorted(Comparator.comparing(Product::getFatt).reversed())
                .map(bar -> bar.getSN() + " ---- " + bar.getFatt())
                .collect(Collectors.toList());
        sortedList.forEach(System.out::println);
    }
}
