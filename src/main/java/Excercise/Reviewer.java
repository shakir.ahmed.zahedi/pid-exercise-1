package Excercise;

import java.time.LocalDate;
/**
 * This is a simple reviewer class.
 * this class maintains information of the review.
 * @author  Shakir Ahmed Zahedi
 * @version 1.0
 * @since   2020-03-16
 */

public class Reviewer {
   private String personID;
   private LocalDate date;
   private int score;

    /**
     * This is a Constructor for object Reviewer.
     * Set personID,date,score for Object Reviewer
     * is constructed.
     * @constructor
     */

    public Reviewer(String personID, LocalDate date, int score){
        this.personID= personID;
        this.date = date;
        this.score = score;

    }

    /**
     * This method
     * @return String
     * for the reviewer ID.
     *
     */

    public String getPersonID() {
        return personID;
    }
    /**
     * This method takes
     * @param  personID string
     * and set for the reviewer personID.
     *
     */
    public void setPersonID(String personID) {
        this.personID = personID;
    }
    /**
     * This method
     * @return Local date
     * for the review date.
     *
     */

    public LocalDate getDate() {
        return date;
    }
    /**
     * This method takes
     * @param  date LocalDate
     * and set for the review date.
     *
     */

    public void setDate(LocalDate date) {
        this.date = date;
    }
    /**
     * This method
     * @return int
     * for the review score.
     *
     */
    public int getScore() {
        return score;
    }
    /**
     * This method takes
     * @param  score int
     * and set for the review score.
     *
     */

    public void setScore(int score) {
        this.score = score;
    }
    /**
     *  This method
     * @override toString method
     * This method
     * @return String
     *for the Reviewer.
     *
     */
    @Override
    public String toString(){
        return " PersonID: "+ personID+ "date: "+ date+ "score: "+ score;

    }


}
