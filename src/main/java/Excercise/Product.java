package Excercise;

import java.util.ArrayList;
/**
 * This is a simple Product class.
 * this class maintains information of the Product.
 * @author  Shakir Ahmed Zahedi
 * @version 1.0
 * @since   2020-03-16
 */

public class Product {
    // fields..
    private String SN;
    private double fatt;
    private double energy;
    private double kolhydrat;
    private double protein;
    private double fiber;
    private ArrayList<Reviewer> reviewers;

    /**
     * This is a Constructor for object Product.
     *
     * @constructor
     */

    public Product(String SN, double fatt, double energy, double kolhydrat, double protein, double fiber, ArrayList<Reviewer> reviewers) {
        this.SN = SN;
        this.fatt = fatt;
        this.energy = energy;
        this.kolhydrat = kolhydrat;
        this.protein = protein;
        this.fiber = fiber;
        this.reviewers = reviewers;

    }


    /**
     * This method
     * @return String
     * for the Product SN.
     *
     */
    public String getSN() {
        return SN;
    }
    /**
     * This method takes
     * @param  SN string
     * and set for the Product SN.
     *
     */
    public void setSN(String SN) {
        this.SN = SN;
    }
    /**
     * This method
     * @return double
     * for the Product Fatt info.
     *
     */
    public double getFatt() {
        return fatt;
    }
    /**
     * This method takes
     * @param  fatt double
     * and set for the Product fatt info.
     *
     */
    public void setFatt(double fatt) {
        this.fatt = fatt;
    }
    /**
     * This method
     * @return double
     * for the Product energy info.
     *
     */
    public double getEnergy() {
        return energy;
    }
    /**
     * This method takes
     * @param  energy double
     * and set for the Product energy info.
     *
     */
    public void setEnergy(double energy) {
        this.energy = energy;
    }
    /**
     * This method
     * @return double
     * for the Product Kolhydrat info.
     *
     */
    public double getKolhydrat() {
        return kolhydrat;
    }
    /**
     * This method takes
     * @param  kolhydrat double
     * and set for the Product kolhydrat info.
     *
     */
    public void setKolhydrat(double kolhydrat) {
        this.kolhydrat = kolhydrat;
    }
    /**
     * This method
     * @return double
     * for the Product protein info.
     *
     */

    public double getProtein() {
        return protein;
    }
    /**
     * This method takes
     * @param  protein double
     * and set for the Product protein info.
     *
     */

    public void setProtein(double protein) {
        this.protein = protein;
    }
    /**
     * This method
     * @return double
     * for the Product Fiber info.
     *
     */
    public double getFiber() {
        return fiber;
    }
    /**
     * This method takes
     * @param  fiber double
     * and set for the Product fiber info.
     *
     */

    public void setFiber(double fiber) {
        this.fiber = fiber;
    }
    /**
     * This method
     * @return ArrayList
     * for the Product review info.
     *
     */
    public ArrayList<Reviewer> getReviewers() {
        return reviewers;
    }
    /**
     * This method takes
     * @param  reviewers ArrayList
     * and set for the Product review info.
     *
     */
    public void setReviewers(ArrayList<Reviewer> reviewers) {
        this.reviewers = reviewers;
    }
    /**
     *  This method
     * @override toString method
     * This method
     * @return String
     *for the Product info .
     *
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        String productinfo = "ProductSN: "+SN+" Fatt:"+fatt+ "Energy: "+energy+ "Kolhydrat: "+ kolhydrat+ "Protein:"+protein
                +"fiber:"+fiber;
        builder.append(productinfo);
        for (Reviewer review : reviewers) {
            builder.append(review.toString());
        }

        return builder.toString();
    }


}
